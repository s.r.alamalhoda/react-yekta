import { Card } from 'react-bootstrap';
import React, {Component} from "react";

class Bootstrap extends Component {
    render() {
        return (
            <Card bg="secondary" text="white" >
                <Card.Header>Header</Card.Header>
                <Card.Body>
                    <Card.Title>Secondary Card Title</Card.Title>
                    <Card.Text>
                        Some quick example text to build on the card title and make up the bulk
                        of the card's content.
                    </Card.Text>
                </Card.Body>
            </Card>
        );
    }
}

export default Bootstrap;
