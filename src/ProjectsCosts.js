import React from 'react';

import API from "./API";
export default class ProjectsCosts extends React.Component {
    state = {
        projetcs: []
    }

    componentDidMount() {
        API.get(`/projectCost`)
            .then(res => {
                const projetcs = res.data;
                this.setState({ projetcs });
                // console.log(projetcs)
            })
    }

    render() {
        return (
            <div>
                <ul>
                    { this.state.projetcs.map(projetc => <li key={projetc.Account_to__name}>{projetc.Account_to__name} : {projetc.amount__sum}</li>)}
                </ul>
            </div>
        )
    }
}
