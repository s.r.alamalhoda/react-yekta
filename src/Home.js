import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import ProjectsCosts from "./ProjectsCosts";
import Bootstrap from "./Botstrap";
import Highcharts1 from "./Highcharts1";
import ProjectsCostYearlyMonthly1 from "./ProjectsCostYearlyMonthly1";
import ProjectsCostYearlyMonthly2 from "./ProjectsCostYearlyMonthly2";
import ProjectsCostYearlyMonthly3 from "./ProjectsCostYearlyMonthly3";
import ProjectsCostCostType1 from "./ProjectsCostCostType1";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));
function Home() {
    const classes = useStyles();
    return (
        <Grid container spacing={3}>
            <Grid item xs={9}>
                <Paper className={classes.paper}><Highcharts1/></Paper>
            </Grid>
            <Grid item xs={3}>
                <Paper className={classes.paper}><ProjectsCosts/></Paper>
            </Grid>
            <Grid item xs={6}>
                <Paper className={classes.paper}><ProjectsCostYearlyMonthly1/></Paper>
            </Grid>
            <Grid item xs={6}>
                <Paper className={classes.paper}><ProjectsCostYearlyMonthly2/></Paper>
            </Grid>
            <Grid item xs={6}>
                <Paper className={classes.paper}><ProjectsCostYearlyMonthly3/></Paper>
            </Grid>
            <Grid item xs={6}>
                <Bootstrap/>
            </Grid>
            <Grid item xs={6}>
                <ProjectsCostCostType1/>
            </Grid>
        </Grid>
    );
}

export default Home;