import {Route, Switch} from "react-router-dom";
import About from "./About";
import Profile from "./Profile";
import Home from "./Home";
import React from "react";

export default function RouterSwitch() {
    return (
        <Switch>
            <Route path="/about">
                <About/>
            </Route>
            <Route path="/profile">
                <Profile/>
            </Route>
            <Route path="/">
                <Home/>
            </Route>
        </Switch>
    )
}