import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import API from "./API";

const getConfig = (data, categories) => ({
    theme : {
        colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572',
            '#FF9655', '#FFF263', '#6AF9C4'],
        chart: {
            backgroundColor: {
                linearGradient: [0, 0, 500, 500],
                stops: [
                    [0, 'rgb(255, 255, 255)'],
                    [1, 'rgb(240, 240, 255)']
                ]
            },
        },
        title: {
            style: {
                color: '#cb5248',
                font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
            }
        },
        subtitle: {
            style: {
                color: '#666666',
                font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
            }
        },
        legend: {
            itemStyle: {
                font: '9pt Trebuchet MS, Verdana, sans-serif',
                color: 'black'
            },
            itemHoverStyle:{
                color: 'gray'
            }
        }
    },
    title: {
        text: 'نمودار هزینه های پروژه یکتای یک به تفکیک ماه'
    },
    chart: {
        renderTo: 'container',
        borderWidth: 1,
        plotBorderWidth: 1,
        style: {
            fontFamily: 'tahoma'
        }
    },
    xAxis: {
        categories:categories,
    },
    series: [{
        // name: 'پروژه ها',
        data: data,
        type: 'line', //'column','area','bar',
    }]
});

class projectsCostYearlyMonthly1 extends React.Component {
    constructor() {
        super();
        this.state = {
            dataResults: [],
            categories:[],
        }
    }
    componentDidMount() {
        this.getData();
    }

    getData = ()=>{
        API.get(`/projectsCostYearlyMonthly/?id=1`)
            .then(res => {
                this.parseData(res.data);
            })
    };

    parseData(data){
        // console.log('Data from server : '+ res.data);
        var resData = data;
        var cat=[];
        var ser1=[];
        resData.forEach(function (d) {
            // console.log('Account_to__name: ' + d.Account_to__name + ' ~ amount__sum: ' + d.amount__sum);
            ser1.push([parseInt(d.amount__sum)]);
            cat.push([d.year + "/" + d.month]);
        });
        // var dataResults=[{"a":12},{"a":12},{"a":12},{"a":12},{"b":2}];
        // var dataResults=[["سلام",  33467245040],
        //     ["سید روح الله علم الهدی",  23043622160],
        //     ["\u0633\u0627\u062e\u062a\u0645\u0627\u0646 \u06cc\u06a9\u062a\u0627 3",  43149066320]];
        var dataResults = ser1;
        var categories=cat;
        this.setState({ dataResults,categories });
    }



    render() {
        const {dataResults} = this.state;
        const {categories} = this.state;
        const chartConfig = getConfig(dataResults, categories);
        // console.log('chartConfig : ' +JSON.stringify(chartConfig));
        return (
            <HighchartsReact
                highcharts={Highcharts}
                options={chartConfig}
            />
        );
    }
}

export default projectsCostYearlyMonthly1;