import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import API from "./API";

const getConfig = (data, categories) => ({
    title: {
        text: 'نمودار مجموع هزینه های پروژه ها از ابتدا تا اکنون'
    },
    chart: {
        renderTo: 'container',
        borderWidth: 1,
        plotBorderWidth: 1,
        style: {
            fontFamily: 'tahoma'
        }
    },
    xAxis: {
        categories:categories,
    },
    series: [{
        // name: 'پروژه ها',
        data: data,
        type: 'column', //'line','area','bar',
    }]
});

class Highcharts1 extends React.Component {
    constructor() {
        super();
        this.state = {
            dataResults: [],
            categories:[],
        }
    }
    componentDidMount() {
        this.getData();
    }

    getData = ()=>{
        API.get(`/projectCost`)
            .then(res => {
                // console.log('Data from server : '+ res.data);
                var resData = res.data;
                var dd=[];
                var cat=[];
                resData.forEach(function (d) {
                    // console.log('Account_to__name: ' + d.Account_to__name + ' ~ amount__sum: ' + d.amount__sum);
                    dd.push([parseInt(d.amount__sum)]);
                    cat.push([d.Account_to__name]);
                });
                // var dataResults=[{"a":12},{"a":12},{"a":12},{"a":12},{"b":2}];
                // var dataResults=[["سلام",  33467245040],
                //     ["سید روح الله علم الهدی",  23043622160],
                //     ["\u0633\u0627\u062e\u062a\u0645\u0627\u0646 \u06cc\u06a9\u062a\u0627 3",  43149066320]];
                var dataResults = dd;
                var categories=cat;
                this.setState({ dataResults,categories });

            })

    }



    render() {
        const {dataResults} = this.state;
        const {categories} = this.state;
        const chartConfig = getConfig(dataResults, categories);
        // console.log('chartConfig : ' +JSON.stringify(chartConfig));
        return (
            <HighchartsReact
                highcharts={Highcharts}
                options={chartConfig}
            />
        );
    }
}

export default Highcharts1;