import ListItem from "@material-ui/core/ListItem";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import List from "@material-ui/core/List";
import React from "react";
import HomeIcon from '@material-ui/icons/Home';

const flexContainer = {
    display: 'flex',
    flexDirection: 'row',
    padding: 0,
};
export default function Menu() {
    return(
    <List style={flexContainer}>
        <ListItem key="home">
            <Button href="#text-buttons" color="inherit" component={Link} to="/home" startIcon={<HomeIcon />}>
                Home
            </Button>
        </ListItem>
        <ListItem key="profile">
            <Button href="#text-buttons" color="inherit" component={Link} to="/profile">
                Profile
            </Button>
        </ListItem>
        <ListItem key="about">
            <Button href="#text-buttons" color="inherit" component={Link} to="/about">
                about
            </Button>
        </ListItem>
    </List>
    )
}