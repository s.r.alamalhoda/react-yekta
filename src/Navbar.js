import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import {  Link } from "react-router-dom";

function NavBar(props) {

    const preventDefault = event => event.preventDefault();
    return (
        // <Router>
        <List>
            <ListItem>
                <Button href="#text-buttons" color="inherit">
                    Home
                </Button>
            </ListItem>
            <ListItem>
                <Button href="#text-buttons" color="inherit" component={Link} to="/profile">
                    Profile
                </Button>
            </ListItem>
            <ListItem>
                <Button href="#text-buttons" color="inherit">
                    Link
                </Button>
            </ListItem>
        </List>
        //
        //     {/*<Route path="/" exact component={Home} />*/}
        //     {/*<Route path="/profile"  component={Profile} />*/}
        //     <Switch>
        //         <Route path="/profile">
        //             <Profile />
        //         </Route>
        //         {/*<Route path="/users">*/}
        //         {/*    <Users />*/}
        //         {/*</Route>*/}
        //         {/*<Route path="/">*/}
        //         {/*    <Home />*/}
        //         {/*</Route>*/}
        //     </Switch>
        // </Router>

    );

}


export default NavBar;