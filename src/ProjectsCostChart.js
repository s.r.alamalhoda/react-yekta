import * as React from 'react';
import API from "./API";
import {Chart, ArgumentAxis, ValueAxis,  BarSeries} from "@devexpress/dx-react-chart-material-ui";
// import { Chart, ArgumentAxis, ValueAxis, LineSeries } from "@devexpress/dx-react-chart-bootstrap4";


const data = [
    { year: '1950', population: 2.525 },
    { year: '1960', population: 3.018 },
    { year: '1970', population: 3.682 },
    { year: '1980', population: 4.440 },
    { year: '1990', population: 5.310 },
    { year: '2000', population: 6.127 },
    { year: '2010', population: 6.930 },
];
export default class ProjectsCostChart extends React.Component {
    state = {
        projetcs: []
    }

    componentDidMount() {
        API.get(`/projectCost`)
            .then(res => {
                const projetcs = res.data;
                this.setState({ projetcs });
                // console.log(projetcs)
            })
    }

    render() {
        return (
            <Chart data={this.state.projetcs}>
            {/*<Chart data={[*/}
            {/*    { argument: 1, value: 10 },*/}
            {/*    { argument: 2, value: 10 },*/}
            {/*    { argument: 3, value: 40 },*/}
            {/*    { argument: 4, value: 40 }*/}
            {/*]}>*/}
                <ArgumentAxis/>
                <ValueAxis/>
                <BarSeries valueField="amount__sum" argumentField="Account_to__name"/>
                {/*<BarSeries valueField="value" argumentField="argument"/>*/}
            </Chart>
        );
    }
}