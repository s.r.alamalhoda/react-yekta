import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import API from "./API";

const getConfig = (data) => ({
    title: {
        text: 'نمودار هزینه های پروژه یکتای یک به تفکیک نوع هزینه'
    },
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        borderWidth: 1,
        style: {
            fontFamily: 'tahoma'
        },
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
            }
        }
    },
    series: [{
        // name: 'پروژه ها',
        data: data,
        type: 'pie', //'column','area','bar','line'
    }]
});

class ProjectsCostCostType1 extends React.Component {
    constructor() {
        super();
        this.state = {
            dataResults: [],
        }
    }
    componentDidMount() {
        this.getData();
    }

    getData = ()=>{
        API.get(`/projectsCostCostType/?id=1`)
            .then(res => {
                this.parseData(res.data);
            })
    };

    parseData(data){
        // console.log('Data from server : '+ res.data);
        var resData = data;
        var cat=[];
        var ser1=[];
        resData.forEach(function (d) {
            // console.log('Account_to__name: ' + d.Account_to__name + ' ~ amount__sum: ' + d.amount__sum);
            ser1.push({name : d.costType__name , y:parseInt(d.amount__sum)});
            // cat.push([d.year + "/" + d.month]);
        });
        // var dataResults=[{"a":12},{"a":12},{"a":12},{"a":12},{"b":2}];
        // var dataResults=[["سلام",  33467245040],
        //     ["سید روح الله علم الهدی",  23043622160],
        //     ["\u0633\u0627\u062e\u062a\u0645\u0627\u0646 \u06cc\u06a9\u062a\u0627 3",  43149066320]];
        var dataResults = ser1;
        this.setState({ dataResults });
    }



    render() {
        const {dataResults} = this.state;
        const {categories} = this.state;
        const chartConfig = getConfig(dataResults, categories);
        console.log('chartConfig : ' +JSON.stringify(chartConfig));
        return (
            <HighchartsReact
                highcharts={Highcharts}
                options={chartConfig}
            />
        );
    }
}

export default ProjectsCostCostType1;